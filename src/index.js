import Masto from 'masto-id-connect';

/**
 * @module masto-auth
 */

/**
 * Class representing the mastodon instance
 */
export class Issuer {
	#issuer
	#masto

	constructor(details, mastoOptions) {
		this.#issuer = details;
		this.#masto = Masto(mastoOptions);
	}

	static async discover(url, mastoOptions){
		return new Issuer(await Masto(mastoOptions).discoverIssuer(url), mastoOptions);
	}

	async registerClient(clientOptions) {
		return new Client({
			issuer: this,
			client: await this.#masto.registerClient(this.#issuer, clientOptions)
		});
	}

	getAuthUrl(client) {
		return this.#masto.getAuthUrl(this.#issuer, client);
	}

	exchangeCode(client, code) {
		return this.#masto.exchangeToken(this.#issuer, client, code);
	}

	getUserInfo(token) {
		return this.#masto.getUserInfo(this.#issuer, token);
	}

	toJSON(){
		return this.#issuer;
	}
}

/**
 * Class representing an app registration against the instance's API
 */
export class Client {
	#issuer
	#client

	constructor(options, mastoOptions){
		if(options instanceof Client) {
			return options;
		}

		let {issuer, client} = options;

		if(!(issuer instanceof Issuer)) {
			issuer = new Issuer(issuer, mastoOptions);
		}
		this.#issuer = issuer;
		this.#client = client;
	}

	// get redirectPath() {
	// 	return new URL(this.#client.redirect_uri).pathname;
	// }

	toJSON(){
		return {
			issuer: this.#issuer.toJSON(),
			client: this.#client
		};
	}

	getAuthUrl(){
		return this.#issuer.getAuthUrl(this.#client);
	}

	exchangeCode(code) {
		return this.#issuer.exchangeCode(this.#client, code);
	}
	getUserInfo(token){
		return this.#issuer.getUserInfo(token);
	}
}


/**
 * Manage mastodon authentication
 * @alias module:masto-auth.default
 */
export default class Auth {
	#getClient
	#mastoOptions

	/**
	 * Create a new instance of Auth
	 * @param {Function} getClient Function that returns the serialized client (i.e. the result of calling Client#toJSON)
	 */
	constructor(gc, mastoOptions){
		this.#mastoOptions = Masto(mastoOptions);
		this.#getClient = async function(url){
			const {origin} = new URL(url);
			const client = await gc(origin);
			if(client) {
				return new Client(client, mastoOptions);
			} else {
				throw new UnregisteredClientError(`No client found for ${origin}`);
			}
		};
	}

	/**
	 * Register with a mastodon instance and return a new instance of Client
	 * @param {string} url The URL of the mastodon instance (any part other than the origin will be ignored)
	 * @param {Object} options Client options
	 * @param {string} options.redirectUri The URI to redirect the user to after they have authenticated on their mastodon instance.
	 * @param {string} options.clientName The name of your application
	 * @returns {Client}
	 * @alias module:masto-auth.default.register
	 */
	static async register(url, options, mastoOptions){
		const issuer = await Issuer.discover(url, mastoOptions);
		return issuer.registerClient(options);
	}

	/**
	 * Get the authentication URL for an issuer
	 * @param {string} url URL of issuer
	 * @returns {string}
	 * @alias module:masto-auth.default#getRedirectUrl
	 */
	async getRedirectUrl(url){
		const client = await this.#getClient(url);
		return client.getAuthUrl();
	}

	/**
	 * Get the user info object for a user who has obtained an authentication code
	 * @param {string} url The URL of the issuer
	 * @param {string} code The code returned from the user auth flow
	 * @returns {Object}
	 * @alias module:masto-auth.default#getUserInfo
	 */
	async getUserInfo(url, code){
		const client = await this.#getClient(url);
		return client.getUserInfo(await client.exchangeCode(code));
	}

	/**
	 * Get the user info object from an auth callback request.
	 * Parse the issuer url and code from a callback request and call getUserInfo
	 * @param {http.IncommingRequest} req Callback request
	 * @return {Object}
	 * @alias module:masto-auth.default#getUserFromCallback
	 */
	async getUserFromCallback(req) {
		const {issuer, code} = Masto(this.#mastoOptions).parseResponse(req);

		return this.getUserInfo(issuer.replace(/^(?![^:]+:)/, 'https://'), code);
	}

	// async getCallbackPath(issuer){
	// 	const c  = await this.#getClient(issuer);
	// 	return c.redirectPath;
	// }
}

/**
 * Error thrown when no client can be found for a given issuer
 */
export class UnregisteredClientError extends Error {}
