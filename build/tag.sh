export VERSION=$(node -e "console.log(require('./package.json').version)")
git tag v$VERSION
if [ $? -eq 0 ]; then
	echo "Tagged as v$VERSION"
else
	echo "Tag v$VERSION already exists"
fi
