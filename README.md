# Masto Auth

A simple library for authenticating against mastodon.

Doesn't provide API access, only verifies who the user is.

Use it to provide OIDC-like auth/login services for mastodon users on your own site.

If you need integration with the mastodon API, try
looking at a mastodon API client instead.

```javascript
import Auth, {UnregisteredClientError} from 'masto-auth';

// Provide a name for your client and the URL to redirect to after the user has authenticated on mastodon.
const clientOptions = {
	client_name: 'My app',
	redirect_uri: 'http://example.com/auth'
}

// Register one or more clients
// Serialise with toJSON and save the details somewhere
// like a database or environment variable
const ms = (await Auth.register('https://mastodon.social', clientOptions)).toJSON();
const kk = (await Auth.register('https://kith.kitchen', clientOptions)).toJSON();

// Create an instance of Auth with a function that retrieves your clients
const auth = new Auth(url => {
	switch(url){
		case: 'https://mastodon.social':
			return ms;
		case: 'https://kith.kitchen':
			return kk;
		default:
			// If no client matches, return null to throw an UnregisteredClientError
			return null;
	}
});

export default async (req, res) => {
	const { pathname } = new URL('file://' + req.url);

	// Get the login URL for each client -
	// here I've hard-coded the instance URLs but
	// you could use a form input to get them from the user
	if(pathname === '/login/kith.kitchen') {
		res.end(await auth.getRedirectUrl('https://kith.kitchen'));
	} else if(pathname === '/login/mastodon.social') {
		res.end(await auth.getRedirectUrl('https://mastodon.social'));

	// This is the auth endpoint we specified in clientOptions - get the user object and do whatever you want with it.
	} else if(pathname === '/auth') {
		res.end(JSON.stringify(await auth.getUserFromCallback(req)))
	}
}
```

## Dependencies
 - masto-id-connect: ^1.1.1
<a name="module_masto-auth"></a>

## masto-auth

* [masto-auth](#module_masto-auth)
    * [.Issuer](#module_masto-auth.Issuer)
    * [.Client](#module_masto-auth.Client)
    * [.default](#module_masto-auth.default)
        * [new module.exports(getClient)](#new_module_masto-auth.default_new)
        * _instance_
            * [.getRedirectUrl(url)](#module_masto-auth.default+getRedirectUrl) ⇒ <code>string</code>
            * [.getUserInfo(url, code)](#module_masto-auth.default+getUserInfo) ⇒ <code>Object</code>
            * [.getUserFromCallback(req)](#module_masto-auth.default+getUserFromCallback) ⇒ <code>Object</code>
        * _static_
            * [.register(url, options)](#module_masto-auth.default.register) ⇒ <code>Client</code>
    * [.UnregisteredClientError](#module_masto-auth.UnregisteredClientError)

<a name="module_masto-auth.Issuer"></a>

### masto-auth.Issuer
Class representing the mastodon instance

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  
<a name="module_masto-auth.Client"></a>

### masto-auth.Client
Class representing an app registration against the instance's API

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  
<a name="module_masto-auth.default"></a>

### masto-auth.default
Manage mastodon authentication

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  

* [.default](#module_masto-auth.default)
    * [new module.exports(getClient)](#new_module_masto-auth.default_new)
    * _instance_
        * [.getRedirectUrl(url)](#module_masto-auth.default+getRedirectUrl) ⇒ <code>string</code>
        * [.getUserInfo(url, code)](#module_masto-auth.default+getUserInfo) ⇒ <code>Object</code>
        * [.getUserFromCallback(req)](#module_masto-auth.default+getUserFromCallback) ⇒ <code>Object</code>
    * _static_
        * [.register(url, options)](#module_masto-auth.default.register) ⇒ <code>Client</code>

<a name="new_module_masto-auth.default_new"></a>

#### new module.exports(getClient)
Create a new instance of Auth


| Param | Type | Description |
| --- | --- | --- |
| getClient | <code>function</code> | Function that returns the serialized client (i.e. the result of calling Client#toJSON) |

<a name="module_masto-auth.default+getRedirectUrl"></a>

#### default.getRedirectUrl(url) ⇒ <code>string</code>
Get the authentication URL for an issuer

**Kind**: instance method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | URL of issuer |

<a name="module_masto-auth.default+getUserInfo"></a>

#### default.getUserInfo(url, code) ⇒ <code>Object</code>
Get the user info object for a user who has obtained an authentication code

**Kind**: instance method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | The URL of the issuer |
| code | <code>string</code> | The code returned from the user auth flow |

<a name="module_masto-auth.default+getUserFromCallback"></a>

#### default.getUserFromCallback(req) ⇒ <code>Object</code>
Get the user info object from an auth callback request.
Parse the issuer url and code from a callback request and call getUserInfo

**Kind**: instance method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| req | <code>http.IncommingRequest</code> | Callback request |

<a name="module_masto-auth.default.register"></a>

#### default.register(url, options) ⇒ <code>Client</code>
Register with a mastodon instance and return a new instance of Client

**Kind**: static method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | The URL of the mastodon instance (any part other than the origin will be ignored) |
| options | <code>Object</code> | Client options |
| options.redirectUri | <code>string</code> | The URI to redirect the user to after they have authenticated on their mastodon instance. |
| options.clientName | <code>string</code> | The name of your application |

<a name="module_masto-auth.UnregisteredClientError"></a>

### masto-auth.UnregisteredClientError
Error thrown when no client can be found for a given issuer

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  
