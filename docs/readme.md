# Masto Auth

A simple library for authenticating against mastodon.

Doesn't provide API access, only verifies who the user is.

Use it to provide OIDC-like auth/login services for mastodon users on your own site.

If you need integration with the mastodon API, try
looking at a mastodon API client instead.

```javascript
import Auth, {UnregisteredClientError} from 'masto-auth';

// Provide a name for your client and the URL to redirect to after the user has authenticated on mastodon.
const clientOptions = {
	client_name: 'My app',
	redirect_uri: 'http://example.com/auth'
}

// Register one or more clients
// Serialise with toJSON and save the details somewhere
// like a database or environment variable
const ms = (await Auth.register('https://mastodon.social', clientOptions)).toJSON();
const kk = (await Auth.register('https://kith.kitchen', clientOptions)).toJSON();

// Create an instance of Auth with a function that retrieves your clients
const auth = new Auth(url => {
	switch(url){
		case: 'https://mastodon.social':
			return ms;
		case: 'https://kith.kitchen':
			return kk;
		default:
			// If no client matches, return null to throw an UnregisteredClientError
			return null;
	}
});

export default async (req, res) => {
	const { pathname } = new URL('file://' + req.url);

	// Get the login URL for each client -
	// here I've hard-coded the instance URLs but
	// you could use a form input to get them from the user
	if(pathname === '/login/kith.kitchen') {
		res.end(await auth.getRedirectUrl('https://kith.kitchen'));
	} else if(pathname === '/login/mastodon.social') {
		res.end(await auth.getRedirectUrl('https://mastodon.social'));

	// This is the auth endpoint we specified in clientOptions - get the user object and do whatever you want with it.
	} else if(pathname === '/auth') {
		res.end(JSON.stringify(await auth.getUserFromCallback(req)))
	}
}
```
