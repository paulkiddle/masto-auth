<a name="module_masto-auth"></a>

## masto-auth

* [masto-auth](#module_masto-auth)
    * [.Issuer](#module_masto-auth.Issuer)
    * [.Client](#module_masto-auth.Client)
    * [.default](#module_masto-auth.default)
        * [new module.exports(getClient)](#new_module_masto-auth.default_new)
        * _instance_
            * [.getRedirectUrl(url)](#module_masto-auth.default+getRedirectUrl) ⇒ <code>string</code>
            * [.getUserInfo(url, code)](#module_masto-auth.default+getUserInfo) ⇒ <code>Object</code>
            * [.getUserFromCallback(req)](#module_masto-auth.default+getUserFromCallback) ⇒ <code>Object</code>
        * _static_
            * [.register(url, options)](#module_masto-auth.default.register) ⇒ <code>Client</code>
    * [.UnregisteredClientError](#module_masto-auth.UnregisteredClientError)

<a name="module_masto-auth.Issuer"></a>

### masto-auth.Issuer
Class representing the mastodon instance

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  
<a name="module_masto-auth.Client"></a>

### masto-auth.Client
Class representing an app registration against the instance's API

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  
<a name="module_masto-auth.default"></a>

### masto-auth.default
Manage mastodon authentication

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  

* [.default](#module_masto-auth.default)
    * [new module.exports(getClient)](#new_module_masto-auth.default_new)
    * _instance_
        * [.getRedirectUrl(url)](#module_masto-auth.default+getRedirectUrl) ⇒ <code>string</code>
        * [.getUserInfo(url, code)](#module_masto-auth.default+getUserInfo) ⇒ <code>Object</code>
        * [.getUserFromCallback(req)](#module_masto-auth.default+getUserFromCallback) ⇒ <code>Object</code>
    * _static_
        * [.register(url, options)](#module_masto-auth.default.register) ⇒ <code>Client</code>

<a name="new_module_masto-auth.default_new"></a>

#### new module.exports(getClient)
Create a new instance of Auth


| Param | Type | Description |
| --- | --- | --- |
| getClient | <code>function</code> | Function that returns the serialized client (i.e. the result of calling Client#toJSON) |

<a name="module_masto-auth.default+getRedirectUrl"></a>

#### default.getRedirectUrl(url) ⇒ <code>string</code>
Get the authentication URL for an issuer

**Kind**: instance method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | URL of issuer |

<a name="module_masto-auth.default+getUserInfo"></a>

#### default.getUserInfo(url, code) ⇒ <code>Object</code>
Get the user info object for a user who has obtained an authentication code

**Kind**: instance method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | The URL of the issuer |
| code | <code>string</code> | The code returned from the user auth flow |

<a name="module_masto-auth.default+getUserFromCallback"></a>

#### default.getUserFromCallback(req) ⇒ <code>Object</code>
Get the user info object from an auth callback request.
Parse the issuer url and code from a callback request and call getUserInfo

**Kind**: instance method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| req | <code>http.IncommingRequest</code> | Callback request |

<a name="module_masto-auth.default.register"></a>

#### default.register(url, options) ⇒ <code>Client</code>
Register with a mastodon instance and return a new instance of Client

**Kind**: static method of [<code>default</code>](#module_masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | The URL of the mastodon instance (any part other than the origin will be ignored) |
| options | <code>Object</code> | Client options |
| options.redirectUri | <code>string</code> | The URI to redirect the user to after they have authenticated on their mastodon instance. |
| options.clientName | <code>string</code> | The name of your application |

<a name="module_masto-auth.UnregisteredClientError"></a>

### masto-auth.UnregisteredClientError
Error thrown when no client can be found for a given issuer

**Kind**: static class of [<code>masto-auth</code>](#module_masto-auth)  
