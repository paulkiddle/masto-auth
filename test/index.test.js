import Auth, {Client, Issuer, UnregisteredClientError} from '../src/index.js';
import {jest} from '@jest/globals';

test('Auth.register', async ()=>{
	const discover = Issuer.discover;
	const issuer = {
		registerClient: jest.fn()
	};
	Issuer.discover = jest.fn(()=>issuer);

	await Auth.register('url', { options: 'options' });

	expect(Issuer.discover).toHaveBeenCalledWith('url', void 0);
	expect(issuer.registerClient).toHaveBeenCalledWith({options:'options'});

	Issuer.discover = discover;
});

test('Auth#getRedirectUrl', async ()=>{
	const issuer = new Issuer;
	issuer.getAuthUrl = jest.fn(()=>'Auth url');

	const getClient = jest.fn(()=>({issuer, client:{}}));
	const auth = new Auth(getClient);

	expect(await auth.getRedirectUrl('http://example.com')).toEqual('Auth url');

	expect(getClient).toHaveBeenCalledWith('http://example.com');
});

test('Auth#getRedirect(Url) on invalid client', async ()=>{
	const getClient = jest.fn(()=>(null));
	const auth = new Auth(getClient);

	expect(auth.getRedirectUrl('http://example.com')).rejects.toThrow(UnregisteredClientError);

	expect(getClient).toHaveBeenCalledWith('http://example.com');
});

test('Get user info from callback', async()=>{
	const req = {
		headers: {
			host: 'localhost',
		},
		url: '?code=1234&issuer=http://example.com'
	};

	const masto = {
		request(){
			return {
				end(){
					return this;
				},
				on(name, cb){
					cb([JSON.stringify({
						url: 'http://example.com/@paul',
						display_name: 'paul',
						id: 'http://example.com/@paul',
						preferredUsername: 'paul',
						icon: 'img.jpg'
					})]);
				}
			};
		}
	};
	const issuer = new Issuer({
		token_endpoint: 'http://example.com/token_endpoint'
	}, masto);
	const client = 'client';
	const getClient = jest.fn(()=>({issuer, client}));

	const auth = new Auth(getClient);

	expect(await auth.getUserFromCallback(req)).toMatchSnapshot();
	expect(getClient).toHaveBeenCalledWith('http://example.com');
});

test('Client to JSON', async()=>{
	const issuer = {a:'b'};
	const client = {c:'d'};

	expect(new Client({issuer, client}).toJSON()).toEqual({issuer, client});
});

test('Discover', async ()=>{
	const masto = {
		request(){
			return {
				end(){
					return this;
				},
				on(name, cb){
					cb([JSON.stringify({
						uri: 'example.com'
					})]);
				}
			};
		}
	};

	const i = await Issuer.discover('http://example.com', masto);

	expect(i).toBeInstanceOf(Issuer);
	expect(i.toJSON()).toMatchSnapshot();
});


test('Register Client', async ()=>{
	const masto = {
		request(){
			return {
				end(){
					return this;
				},
				on(name, cb){
					cb([JSON.stringify({
						client: 'client'
					})]);
				}
			};
		}
	};

	const issuer = new Issuer({ issuer: 'http://example.com'}, masto);

	const client = await issuer.registerClient({ redirectUri: 'http://example.com' });

	expect(client).toBeInstanceOf(Client);
	expect(client.toJSON()).toMatchSnapshot();
});


test('Get auth URL', async ()=>{
	const issuer = new Issuer({
		authorization_endpoint: 'auth_ep'
	});

	expect(issuer.getAuthUrl({
		client_id: 'client_id',
		redirect_uri: 'redirect_uri'
	})).toEqual('auth_ep?scope=read&client_id=client_id&redirect_uri=redirect_uri&response_type=code');
});

test('Client accepts client', async() =>{
	const c = new Client({});

	expect(new Client(c)).toBe(c);
});
